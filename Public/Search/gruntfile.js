module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    sass: {
      dist: {
        files: [
          {
            src: ['Template/css/core.sass'],
            dest: 'Template/css/core.css'
          }
        ]
      }
    },

    concat: {
      css: {
          src: 'Template/css/Uncompressed/*.sass',

          dest: 'Template/css/core.sass'
      },

      js: {
          src: ['Template/js/Uncompressed/*.js'],
          dest: 'Template/js/core.js'
      }
    },

    watch: {
      css: {
        files: 'Template/css/Uncompressed/*.sass',
        tasks: ['concat:css', 'sass']
      },

      js: {
        files: 'Template/js/Uncompressed/*.js',
        tasks: ['concat:js']
      },
    }

  });

  grunt.registerTask('default', ['watch']);
  grunt.registerTask('css', ['concat:css', 'sass']);
  grunt.registerTask('js', ['concat:js']);

}
