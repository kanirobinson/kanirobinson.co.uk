/* Scroll Event for Header Fixed */
var offset = $("nav.navigation").offset().top;

$(window).on("scroll", function(e){
  if($(this).scrollTop() > offset){
    $("nav.navigation").addClass("scrolling");
  }

  if(offset > $(this).scrollTop()){
    $("nav.navigation").removeClass("scrolling");
  }
});
/* End Scroll Event for Header Fixed */

/* Click Event for Chevron Down */
$(document).on("click", "section.work", function(){
  $(".myWork").animate({
    "scrollTop": $("section.work").offset().left
  });
});
  /* End Click Event for Chevron Down */
$(document).on("click", "a", function(e){
  e.preventDefault();
  var _this = $(this);
  $(".loader span").animate({"width" : "75%"}, 1000);
  $("div.main").load(_this.attr("href") + ' .fullPage', function(){
    $(".loader span").animate({"width" : "100%"});
  });
});
