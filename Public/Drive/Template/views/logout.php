<?php
	if(isset($_SESSION['user']['id'])) {
		session_destroy();
		header("Location: https://admin.kanirobinson.co.uk/index");
	} else {
		header("Location: https://kanirobinson.co.uk/index");
	}