<!DOCTYPE html>
<html>
  <head>
    <title>Kani Robinson: Blog</title>
    <base href="https://<?=$_SERVER['HTTP_HOST']?>/Template/">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/core.css">
  </head>
  <body>
    <nav class="navigation noMargin">
      <div class="wrapper">
        <a href="/index"><img src="images/logo.png" height="35" style="float: left; margin-top: 10px;" alt="logo" /></a>
        <i class="fa fa-search right" style="float: right; font-size: 24px; margin: 15px 0 0 0; cursor: pointer;" aria-hidden="true"></i>
        <i class="fa fa-ellipsis-h right" style="float: right; font-size: 24px; cursor: pointer; margin: 15px 20px 0 0;" aria-hidden="true"></i>
      </div>
    </nav>
    <div class="loader"><span></span></div>
    <div class="main">
        <div class="fullPage">
