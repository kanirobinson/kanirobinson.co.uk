<section class="mast">  
  <div class="wrapper">
    <h1 class="mastText">Hello there, I am Kani</h1>
    <p class="mastText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent.<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    <section class="social">
      <ul class="socialList clearfix">
        <li>
          <a href="https://facebook.com/kanirobinson" class="noDecoration" target="_blank">
            <i class="fa fa-facebook" aria-hidden="true"></i>
          </a>
        </li>
        <li>
          <a href="https://twitter.com/kanitrobinson" class="noDecoration" target="_blank">
            <i class="fa fa-twitter" aria-hidden="true"></i>
          </a>
        </li>
        <li>
          <a href="https://www.instagram.com/kanirobinson/" class="noDecoration" target="_blank">
            <i class="fa fa-instagram" aria-hidden="true"></i>
          </a>
        </li>
        <li>
          <a href="https://uk.linkedin.com/in/kani-robinson-50a00ab9" class="noDecoration" target="_blank">
            <i class="fa fa-linkedin" aria-hidden="true"></i>
          </a>
        </li>
        <li>
          <a href="https://github.com/KaniRobinson" class="noDecoration" target="_blank">
            <i class="fa fa-github-alt" aria-hidden="true"></i>
          </a>
        </li>
      </ul>
    </section>
  </div>
</section>
<section class="blog">
  <ul class="blog">
  <?php
    foreach($Blog->output() as $Key => $Val) {
      switch($Val['type']) {
        case "post":
          echo'
            <li class="' . $Val['type'] . '">
              <div class="wrapper">
                <a href="/post/' . $Val['id'] . '">
                  <img src="' . $Val['avatar'] . '" class="avatar" alt="" />
                  <p class="dateTime">' . $Val['dateTime'] . '</p>
                  <p class="description">
                   <span class="dropCap">' . strip_tags(trim($Val['description']))[0] . '</span>' . substr(strip_tags(trim($Val['description'])), 1, 300) . '...
                  </p>
                </a>
              </div>
            </li>
          ';
        break;
        case "image":
          echo'
            <li class="' . $Val['type'] . '" style="background-image: url(\'' . $Val['background'] . '\');">
              <div class="overlay"></div>
              <div class="wrapper">
                <div class="center">
                  <div class="labels">
                  ';
                  foreach($Val['labels'] as $key => $val){
                    echo '<a href="/post/' . $Val['id'] . '">#' . $val . '</a>';
                  }
                  echo'
                  </div>
                  <a href="/post/' . $Val['id'] . '" class="title">' . $Val['title'] . '</a>
                </div>
              </div>
            </li>
          ';
        break;
      }

    }
  ?>
  </ul>
</section>
