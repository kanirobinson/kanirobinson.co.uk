<?php
  $data = (isset($Blog->output()[$_GET['id'] - 1])) ? $Blog->output()[$_GET['id'] - 1] : die("This Post is not avaliable");
?>
<section class="mast" style="background-image: url(<?=$data['background']?>);">
  <div class="wrapper">
    <h1 class="mastText"><?=$data['title']?></h1>
    <p class="mastText"><?=$data['subtitle']?></p>
  </div>
</section>
<section class="blog">
  <ul class="blog">
    <li class="post">
      <div class="wrapper">
        <img src="http://4.bp.blogspot.com/-gxeFk6vVX_o/VlKnYTkuF-I/AAAAAAAAA0M/a3hhvPMKmbY/s113/PP4.jpg" class="avatar" alt="" />
        <p class="dateTime"><?=$data['dateTime']?></p>
        <?=$data['description']?>
      </div>
    </li>
  </ul>
</section>
