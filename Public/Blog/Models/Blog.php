<?php
  namespace Models;

  class Blog {
    private static $data = [];

    public function __CONSTRUCT()
    {}

    public static function getPosts()
    {
      global $Database;

      $Blog = [];
      $select = $Database::$Main->prepare("SELECT
                                          blog.id, blog.cover, blog.title,
                                          blog.subtitle, blog.text, blog.published,
                                          blog.labels
                                          FROM
                                          blog blog
                                          WHERE
                                          active = 1");
      $select->execute();
      $i = 1;
      while($row = $select->fetch(\PDO::FETCH_OBJ)){
        $type = ($i % 2 == 0) ? "image" : "post";
        array_push($Blog, [
          "id" => $row->id,
          "type" => $type,
          "background" => 'https://assets.kanirobinson.co.uk/uploads/blog/' . $row->cover,
          "title" =>  $row->title,
          "subtitle" => $row->subtitle,
          "avatar" => "../../Avatars/dGtlC9V292.jpg",
          "description" => $row->text,
          "dateTime" => date('l, F d, Y', strtotime($row->published)),
          "labels" => unserialize($row->labels),
        ]);
        $i++;
      }

      return $Blog;
    }

    public static function output()
    {
      self::$data = self::getPosts();
      return self::$data;
    }
  }
