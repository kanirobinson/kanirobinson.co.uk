<?php
	$Controller = [
		'title' => 'Admin Login',
		'description' => 'Login to the administration side of your website',
		'keywords' => 'Keywords, KaniRobinson, Admin',
		'login' => 2,
		'active' => 1,
	];