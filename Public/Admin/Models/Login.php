<?php
  namespace Models;

  class Login {
    private static $data = [
      'success' => false,
      'error' => [
        'email' => null,
        'password' => null,
        'other' => null,
      ]
    ];

    public function __CONSTRUCT() {}

    private static function emailValid()
    {
      global $Database, $_POST;

      $select = $Database::$Main->prepare('SELECT 
                                           email_address
                                           FROM
                                           usersystem
                                           WHERE 
                                           email_address = :email_address');
      $select->execute([
        ':email_address' => $_POST['email']
      ]);

      return ($select->rowCount() > 0) ? true : false;
    }

    private static function passwordValid()
    {
      global $Database, $_POST;

      $select = $Database::$Main->prepare('SELECT 
                                           password
                                           FROM
                                           usersystem
                                           WHERE 
                                           email_address = :email_address');
      $select->execute([
        ':email_address' => $_POST['email'],
      ]);

      $result = $select->fetch(\PDO::FETCH_OBJ);

      return (password_verify($_POST['password'], $result->password)) ? true : false;
    }

    public static function getData()
    {
      global $Database, $_POST;

      $select = $Database::$Main->prepare('SELECT 
                                           id, email_address, username,
                                           fname, lname, profile_picture
                                           FROM
                                           usersystem
                                           WHERE 
                                           email_address = :email_address');
      $select->execute([
        ':email_address' => $_POST['email'],
      ]);

      $result = $select->fetch(\PDO::FETCH_OBJ);

      return $result;
    }

    public static function setSessions()
    {
      foreach(self::getData() as $Key => $Val)
      {
        $_SESSION['user'][$Key] = $Val;
      }
    }

    public static function output() 
    {
      if(!isset($_SESSION['user']['id'])){
        if(isset($_POST['login']) == true) {

          if(self::emailValid() == false) {
            self::$data['error']['email'] = 'Email Address not found';
          }

          if(self::passwordValid() == false) {
            self::$data['error']['password'] = 'Incorrect Password';
          }

          if(self::emailValid() == true && self::passwordValid() == true){
            self::$data['error'] = null;
            self::$data['success'] = true;

            self::setSessions();
          }
        }
      } else {
        self::$data['error']['other'] = 'ERROR';
      }
      
      if(isset($_POST['profilePicture']) == true) {
        if(self::emailValid() == true) {
          self::$data['success'] = self::getData()->profile_picture;
        } else {
          self::$data['error']['email'] = 'Email Address not found';
        }
      }

      return self::$data;
    }
  }
