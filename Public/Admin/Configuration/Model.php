<?php

    $model = [
        'Core' => [
          //'Cache'     =>  'Pluto\Application\Cache\Cache',
          //'Data'      =>  'Pluto\Application\Data\Data',
          'Database'  =>  'Pluto\Application\Database\Database',
          //'Error'     =>  'Pluto\Application\Error\Error',
          //'Form'      =>  'Pluto\Application\Form\Form',
          //'Libraries' =>  'Pluto\Application\Libraries\Libraries',
          //'Model'     =>  'Pluto\Application\Model\Model',
          'Template'  =>  'Pluto\Application\Template\Template',
          'Param'     =>  'Pluto\Application\Template\Param',
        ],
        'Models' => [],
    ];
