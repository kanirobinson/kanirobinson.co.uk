<script>
	var previousPage = "{previous_page}";
</script>
<section class="admin">
	<div class="wrapper">
		<form action="/Methods/login.php" class="loginForm" method="POST">
			<img class="profilePicture" src="http://www.sessionlogs.com/media/icons/defaultIcon.png" alt="default" />
			<h2>Welcome Back!</h2>
			<p>Sign into your account.</p>
			<input type="email" name="email" placeholder="Admin@JohnDoe.com" />
			<i class="fa fa-check emailCheck" aria-hidden="true"></i>
			<input type="password" name="password" placeholder="Password" />
			<i class="fa fa-check passwordCheck" aria-hidden="true"></i>
			<input type="submit" name="login" value="LOGIN" />
		</form>
	</div>
</section>