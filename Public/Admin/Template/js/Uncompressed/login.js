$(function(){
  
  $(document).on("change", "input[name='email']", function(){
    $.ajax({
      type: $("form.loginForm").attr('method'),
      url: $("form.loginForm").attr('action'),
      dataType: 'json', 
      data: {
        email: $("input[name='email']").val(),
        profilePicture: 1
      },
      success: function( response ) {
        if(response.error.email == null) {
          $("img.profilePicture").attr("src", response.success);
        } else {
          $("img.profilePicture").attr("src", 'http://www.sessionlogs.com/media/icons/defaultIcon.png');
        }
      }
    });
  });

  $(document).on("submit", "form.loginForm", function(e){
    e.preventDefault();
    var _this = $(this),
        _email = $("input[name='email']"),
        _password = $("input[name='password']");
      if($("input[name='login']").attr("disabled") != "true"){
        _email.attr("disabled", "true");
        _password.attr("disabled", "true");
        $("input[name='login']").attr("disabled", "true");

        $.ajax({
          type: _this.attr('method'),
          url: _this.attr('action'),
          dataType: 'json',
          data: {
            email: _email.val(),
            password: _password.val(),
            login: 1
          },
          success: function( response ) {
            $(".loginMessage").remove();
            if(response.success == true) {
              _this.before("<div class='loginMessage'>Succesfully Logged in</div>");
              $(".emailCheck, .passwordCheck").removeClass("fa-times").removeClass("failed").addClass("fa-check").addClass("active");

              setTimeout(function(){
                window.location = previousPage;
              }, 1000);
            } else {
                _email.removeAttr("disabled");
                _password.removeAttr("disabled");
                $("input[name='login']").removeAttr("disabled");

                if(response.error.other == null) {
                  if(response.error.email != null) {
                    $(".emailCheck").removeClass("fa-check").removeClass("active").addClass("fa-times").addClass("failed");
                  } else {
                    $(".emailCheck").removeClass("fa-times").removeClass("failed").addClass("fa-check").addClass("active");
                  }
                  if(response.error.password != null) {
                    $(".passwordCheck").removeClass("fa-check").removeClass("active").addClass("fa-times").addClass("failed");
                  } else {
                    $(".passwordCheck").removeClass("fa-times").removeClass("failed").addClass("fa-check").addClass("active");
                  }

                } else {
                  _this.before("<div class='loginMessage error'>" + response.error.other + "</div>");
                }
            }
          }
        });
      }

  });
});