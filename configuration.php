<?php
  $Configuration = [
    'Errors' => E_ALL,
    'https' => true,
    'Configuration' => [
      '/Configuration/Cache.php',
      '/Configuration/Data.php',
      '/Configuration/Database.php',
      '/Configuration/Error.php',
      '/Configuration/Form.php',
      '/Configuration/Libraries.php',
      '/Configuration/Model.php',
      '/Configuration/Template.php',
      '/Controllers/' . $_GET['v'] . '.php',
    ],
    'ini' => [
      'display_errors' => 1,
      'session.cookie_domain' => '.kanirobinson.co.uk',
    ]
  ];
