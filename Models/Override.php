<?php
    namespace Pluto\Models;

    class Override {

        public function __construct()
        {
            self::filemtime();
        }

        public static function filemtime()
        {
            function filemtime($filename)
            {
                clearstatcache();

                filemtime($filename);
            }
        }

    }