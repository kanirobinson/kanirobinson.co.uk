<?php
    namespace Pluto\Application\Template;

    // System Entry Key Validation
    if (!defined("ALLOW_ENTRY_VALIDATION")) {
        die("Pluto: You should not be here.");
    }

    // Template Engine
    class Template {

        public static function Engine($File)
        {
          // Check if Admin is required
	        self::requireAdmin();

          // Check if App is Active
          if(self::Active())
          {
            // Valid Request Check
            if(self::Valid($File))
            {
                // View Existance Check
                if(self::Exists($File))
                {
                    // Load File
                    return self::Load($File);
                }
            }
          }
        }

        public static function requireAdmin()
        {
          global $template, $Controller;

          switch($Controller['login']) {
            case 1:
                $previous = base64_encode("https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
                return (isset($_SESSION['user']['id'])) ? true : header("Location: https://admin.kanirobinson.co.uk/index&previous=" . $previous);
            break;
            case 2:
                return (!isset($_SESSION['user']['id'])) ? true : header("Location: https://admin.kanirobinson.co.uk/dashboard");
            break;
            default:
                return true;
            break;
          }
        }

        public static function Load($File)
        {
	         global $Param, $model;

            // Start output buffer
            ob_start();

            // Get Website Models
            if($File != "Template/structure/header.php" && $File != "Template/structure/footer.php"){
              foreach($model['Models'] as $Key => $Val)
              {
                include $_SERVER['DOCUMENT_ROOT'] . '/' . str_replace('\\', '/', $Val) . '.php';
                ${$Key} = new $Val();
              }
            }

            // Include File
            include $File;

            // Get The Contents
            $template = ob_get_contents();

            // End Buffer
            ob_end_clean();

            //Return the File
            return $Param->engine($template);
        }

        public static function Exists($File)
        {
            // Checks if File Exists
            return (file_exists($File)) ? true : false;
        }

        public static function Valid($File)
        {
            // Checks to See if Request URI is Set or Blank
            return (isset($File) && $_SERVER['REQUEST_URI'] != "/") ? true : header("Location: index");
        }

        public static function Active()
        {
          global $template;
          return ($template['active'] == 1) ? true : header("Location: https://kanirobinson.co.uk");
        }

        public static function Structure($File)
        {
          return self::Load("Template/structure/" . $File . ".php");
        }
    }
