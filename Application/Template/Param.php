<?php
    namespace Pluto\Application\Template;

    class Param {

	public static $params = [];

        public function __construct()
        {
        }

	public static function create($find, $data)
	{
		@self::$params[$find] = $data;
	}

	public static function engine($file)
	{
		foreach(self::$params as $Key => $Val){
			$file  = str_replace('{' . $Key . '}', $Val, $file);
		}

		return $file;
	}
    }
