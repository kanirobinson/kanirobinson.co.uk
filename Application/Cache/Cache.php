<?php
    namespace Pluto\Application\Cache;

    use Pluto\Application\Template\Template;
    
    /* Todo Tasks:
     * 
     * Create Custom File Names.
     *  filemtime of the fileName and .Cache
     *  Set The FileType at the Top of the file.
     *
    */

    class Cache {

        private static $Location = "Cache/";
        private static $Trash = "Trash/";

        public static function getLocation()
        {
            return self::$Location;
        }

        public static function getTrash()
        {
            return self::getLocation() . "Trash/";
        }

        public function __construct($Content, $fileName, $Time=60)
        {

            if(self::Exists($Content, $fileName))
            {
                if(self::timeOut($Time, $fileName))
                {
                    self::Create($Content, $fileName);
                }
            } else {
                self::Create($Content, $fileName);
            }
        }

        // Create Cache File
        public static function Create($Content, $fileName)
        {
            $myfile = fopen(self::getLocation() . $fileName, "w") or die("Unable to open file!");
            fwrite($myfile, $Content);
            fclose($myfile);
        }

        // Exists
        public static function Exists($Content, $fileName)
        {
            if(file_exists(self::getLocation() . $fileName))
            {
                return true;
            } else {
                return false;
            }
        }

        // Cache Timeout
        public static function timeOut($Time, $fileName)
        {
            if(time() - filemtime(self::getLocation() . $fileName) >= $Time)
            {
                return true;
            } else {
                return false;
            }
        }

        // Get Cache
        public static function Get($fileName, $Time=60, $Content="")
        {
            if(self::timeOut($Time, $fileName))
            {
                self::Create($Content, $fileName);
            }

            return Template::Load(self::getLocation() . $fileName);
        }
        

        // Delete Cache
        public static function Remove($fileName, $Content="")
        {
            if(self::Exists($Content, $fileName))
            {
                copy(self::getLocation() . $fileName, self::getTrash() . $fileName);
                self::Delete($fileName);

            }
        }

        public static function Delete($fileName, $Content="")
        {
            if(self::Exists($Content, $fileName))
            {
                unlink(self::getLocation() . $fileName);
            }
        }

        // Recover Cache
        public static function Recover($fileName, $Content="")
        {
            if(self::Exists($Content, self::$Trash . $fileName))
            {
                copy(self::getTrash() . $fileName, self::getLocation() . $fileName);
                self::Delete(self::$Trash . $fileName);

            }
        }
    }