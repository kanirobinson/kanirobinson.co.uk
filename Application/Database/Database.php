<?php
    namespace Pluto\Application\Database;

    class Database {

        public static $Main;
        public static $Slave;

        public function __construct()
        {
	    global $database;

	    self::$Main = self::Connection(
	        $database['host'], 
                $database['username'],
                $database['password'],
                $database['database']
	    );
        }

        public static function Connection($host, $username, $password, $database)
        {
          try {
            $Connection = new \PDO('mysql:host=' . $host . ';dbname=' . $database, $username, $password);
            $Connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
          } catch (\PDOException $e) {
            print "Error " . $e->getMessage();
            die();
          }

          return $Connection;
        }

    }
