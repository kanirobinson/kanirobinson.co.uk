<?php
  // Get Global Configuration File
  include 'configuration.php';

  // Error Reporting
  error_reporting($Configuration['Errors']);

  // Get Ini Settings
  foreach($Configuration['ini'] as $Key => $Val)
  {
    ini_set($Key, $Val);
  }

  // Sessions Manager
  session_set_cookie_params(0, '/', '.kanirobinson.co.uk');
  session_start();

  // Get Configuration Files
  foreach($Configuration['Configuration'] as $Val)
  {
    if(file_exists($_SERVER['DOCUMENT_ROOT'] . $Val)) {
      include $_SERVER['DOCUMENT_ROOT'] . $Val;
    } 
  }

  // Get Core Classes
  foreach($model['Core'] as $Key => $Val)
  {
    include __DIR__ . '/' . str_replace(['Pluto\\', '\\'], ['', '/'], $Val) . '.php';
    ${$Key} = new $Val();
  }
  // Get Params
  foreach($data as $Key => $Val){
      $Param->create($Key, $Val);
  }

  if($Configuration['https'] == true && $_SERVER['HTTP_X_FORWARDED_PROTO'] != "https")
  {
     header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
     exit();
  }
